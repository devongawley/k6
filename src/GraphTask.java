import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   // This will generate a random height for each Vertex
   public static final Random randomNum = new Random();

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {

//      TEST TÜHI GRAAF
//       GraphTask task = new GraphTask();
//       Graph empty = task.new Graph("Tühi Test");
//       empty.getPathWithLowestPeak("D");

//      TEST 1 Punkti graaf
//       GraphTask task = new GraphTask();
//       Graph onePoint = task.new Graph("Ühepunkti Test");
//       Vertex a = new Vertex("A");
//       onePoint.first = a;
//       a.height = 0;
//       System.out.println(onePoint.toString());
//       System.out.println(onePoint.getPathWithLowestPeak("A"));

//      TEST 2 punkti graaf
//       GraphTask task = new GraphTask();
//       Graph twoPoint = task.new Graph("Kahepunkti Test");
//       Vertex a = new Vertex("A");
//       Vertex b = new Vertex("B");
//       Arc ab = new Arc("AB");
//       twoPoint.first = a;
//       a.height = 0;
//       b.height = 8;
//       a.next = b;
//       a.first = ab;
//       ab.target = b;
//       System.out.println(twoPoint.toString());
//       System.out.println(twoPoint.getPathWithLowestPeak("B"));

//       TEST VIISNURK
//       GraphTask task = new GraphTask();
//       Graph g = task.new Graph("Test Viisnurk");
//       Vertex a = task.new Vertex("A");
//       Vertex b = task.new Vertex("B");
//       Vertex c = task.new Vertex("C");
//       Vertex d = task.new Vertex("D");
//       Vertex e = task.new Vertex("E");
//       g.first = a;
//       a.height = 0;
//       b.height = 2;
//       d.height = 2;
//       e.height = 1;
//       c.height = 5;
//       Arc ab = new Arc("AB");
//       Arc bd = new Arc("BD");
//       Arc de = new Arc("DE");
//       Arc ac = new Arc("AC");
//       Arc ce = new Arc("CE");
//       a.first = ab;
//       b.first = bd;
//       d.first = de;
//       c.first = ce;
//       a.next = b;
//       b.next = c;
//       c.next = d;
//       d.next = e;
//
//       ab.next = ac;
//       ab.target = b;
//       bd.target = d;
//       ac.target = c;
//       ce.target = e;
//       de.target = e;
//       System.out.println(g.toString());
//       System.out.println(g.getPathWithLowestPeak("E"));


//      TEST 2000+ tipuga graaf
       GraphTask task = new GraphTask();
       Graph largeGraph = task.new Graph("Suur Graaf");
       long startTime = System.currentTimeMillis();
       largeGraph.createRandomSimpleGraph(2500, 2500);
       System.out.println(largeGraph.getPathWithLowestPeak("v2500"));
       long stopTime = System.currentTimeMillis();
       long elapsedTime = stopTime - startTime;
       System.out.println("Time spent solving the graph: " + elapsedTime + " millisekundit.");

   }

    /**
     * This is an implementation of Breadth-First Search algorithm.
     * Every vertex has a height, which determines if we move to it
     * or not.
     */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int height;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
         height = randomNum.nextInt(10);
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
           this (s, null, null);
      }



       @Override
      public String toString() {
         return id;
      }
   } 


   class Graph {

       private String id;
       private Vertex first;
       public int info = 0;

       Graph(String s, Vertex v) {
           id = s;
           first = v;
       }

       Graph(String s) {
           this(s, null);
       }

       @Override
       public String toString() {
           String nl = System.getProperty("line.separator");
           StringBuffer sb = new StringBuffer(nl);
           sb.append(id);
           sb.append(nl);
           Vertex v = first;
           while (v != null) {
               sb.append(v.toString());
               sb.append("[H:");
               sb.append(v.height);
               sb.append("]");
               sb.append(" -->");
               Arc a = v.first;
               while (a != null) {
                   sb.append(" ");
                   sb.append(a.toString());
                   sb.append(" (");
                   sb.append(v.toString());
                   sb.append("->");
                   sb.append(a.target.toString());
                   sb.append(")");
                   a = a.next;
               }
               sb.append(nl);
               v = v.next;
           }
           return sb.toString();
       }

       public Vertex createVertex(String vid) {
           Vertex res = new Vertex(vid);
           res.next = first;
           first = res;
           return res;
       }

       public Arc createArc(String aid, Vertex from, Vertex to) {
           Arc res = new Arc(aid);
           res.next = from.first;
           from.first = res;
           res.target = to;
           return res;
       }

       /**
        * Create a connected undirected random tree with n vertices.
        * Each new vertex is connected to some random existing vertex.
        *
        * @param n number of vertices added to this graph
        */
       public void createRandomTree(int n) {
           if (n <= 0)
               return;
           Vertex[] varray = new Vertex[n];
           for (int i = 0; i < n; i++) {
               varray[i] = createVertex("v" + String.valueOf(n - i));
               if (i > 0) {
                   int vnr = (int) (Math.random() * i);
                   createArc("a" + varray[vnr].toString() + "_"
                           + varray[i].toString(), varray[vnr], varray[i]);
                   createArc("a" + varray[i].toString() + "_"
                           + varray[vnr].toString(), varray[i], varray[vnr]);
               } else {
               }
           }
       }

       /**
        * Create an adjacency matrix of this graph.
        * Side effect: corrupts info fields in the graph
        *
        * @return adjacency matrix
        */
       public int[][] createAdjMatrix() {
           info = 0;
           Vertex v = first;
           while (v != null) {
               v.info = info++;
               v = v.next;
           }
           int[][] res = new int[info][info];
           v = first;
           while (v != null) {
               int i = v.info;
               Arc a = v.first;
               while (a != null) {
                   int j = a.target.info;
                   res[i][j]++;
                   a = a.next;
               }
               v = v.next;
           }
           return res;
       }

       /**
        * Create a connected simple (undirected, no loops, no multiple
        * arcs) random graph with n vertices and m edges.
        *
        * @param n number of vertices
        * @param m number of edges
        */
       public void createRandomSimpleGraph(int n, int m) {
           if (n <= 0)
               return;
           if (n > 2500)
               throw new IllegalArgumentException("Too many vertices: " + n);
           if (m < n - 1 || m > n * (n - 1) / 2)
               throw new IllegalArgumentException
                       ("Impossible number of edges: " + m);
           first = null;
           createRandomTree(n);       // n-1 edges created here
           Vertex[] vert = new Vertex[n];
           Vertex v = first;

           int c = 0;
           while (v != null) {
               vert[c++] = v;
               v = v.next;
           }
           int[][] connected = createAdjMatrix();
           int edgeCount = m - n + 1;  // remaining edges
           while (edgeCount > 0) {
               int i = (int) (Math.random() * n);  // random source
               int j = (int) (Math.random() * n);  // random target
               if (i == j)
                   continue;  // no loops
               if (connected[i][j] != 0 || connected[j][i] != 0)
                   continue;  // no multiple edges
               Vertex vi = vert[i];
               Vertex vj = vert[j];
               createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
               connected[i][j] = 1;
               createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
               connected[j][i] = 1;
               edgeCount--;  // a new edge happily created
           }
       }

       /**
        * This getter will be used to return the root vertex
        * @return Vertex
        */
       public Vertex getFirst() {
           return first;
       }

       /**
        * This method is for getting a vertex
        * by its ID (string). If not found, the
        * the method throws an exception explaing
        * to the user what went wrong.
        *
        * @param node is the String representation of the id property of a vertex.
        * @return Vertex
        */
       public Vertex getVertexByID(String node) throws RuntimeException {
           Vertex vertex = getFirst();
           do {
               if (vertex.id.equals(node)) {
                   return vertex;
               }
               vertex = vertex.next;
           } while (vertex != null);
           throw new RuntimeException(String.format("The point %s cannot be reached or is not in the graph.", node));
       }

       /**
        * This method returns a boolean with info concerning if
        * the next vertex is the last vertex we are looking for.
        * @param someVertex represents the current vertex we are comparing.
        * @param last represents the vertex we are comparing against.
        * @return boolean if the next vertex is last
        */
       public boolean isNextVertexLast(Vertex someVertex, Vertex last) {
           return someVertex.first.target.equals(last) || (first.first.next != null && first.first.next.target != null && first.first.next.target.equals(last));
       }

       /**
        * This method returns a boolean with info
        * concerning if there is only 1 vertex in a graph
        * @return boolean is there only 1 vertex in graph
        */
       public boolean isOnlyVertexInGraph() {
           return getFirst().first == null && getFirst().next == null;
       }

       /**
        * This finds a path with the lowest peak
        * to a given vertex starting from the root vertex.
        * This will return a LinkedList with the vertices
        * in order and will print out a formatted string.
        * @param node represents the String value of a vertex's id.
        * @return LinkedList containing vertices
        */
       public LinkedList<Vertex> getPathWithLowestPeak(String node) {
           LinkedList<Vertex> lowestPathList = new LinkedList<>();
           if (getFirst() == null) {
               System.out.println("The provided graph has no points. It is empty.");
               return lowestPathList;
           }

           if (isOnlyVertexInGraph()) {
               lowestPathList.add(first);
               System.out.println(first + "[H:" + first.height + "]");
               return lowestPathList;
           }

           Vertex first = getFirst();
           Vertex last = getVertexByID(node);
           lowestPathList.add(first);
           first.info = 1;
           Vertex someVertex = first;

           if (isNextVertexLast(someVertex, last)) {
               lowestPathList.add(last);
               System.out.println(first + "[H:" + first.height + "]" + "->" + last + "[H:" + last.height + "]");
               return lowestPathList;
           }

           try {
               while (!isNextVertexLast(someVertex, last)) {
                   Arc currentArc = someVertex.first;
                   int currentLowestHeight = 100; // A random number larger than any height can be
                   Vertex neighbourWithLowestHeight = null;
                   while (currentArc != null) {
                       if (currentArc.target.info != 1) {
                           currentArc.target.info = 1;
                           if(isNextVertexLast(someVertex, last)) {
                               break;
                           }
                           Vertex neighbouringVertex = currentArc.target;
                           if (neighbouringVertex.height < currentLowestHeight) {
                               currentLowestHeight = neighbouringVertex.height;
                               neighbourWithLowestHeight = neighbouringVertex;
                           }
                       }
                       currentArc = currentArc.next;
                   }
                   lowestPathList.add(neighbourWithLowestHeight);
                   someVertex = neighbourWithLowestHeight;
               }
           } catch (Exception ignored) {
               lowestPathList.removeLast();
           }
           StringBuilder sb = new StringBuilder();
           for (Vertex vertex : lowestPathList) {
               if (vertex != null) {
                   sb.append(vertex).append("[H:").append(vertex.height).append("]").append("->");
               }
           }
           lowestPathList.add(last);
           sb.append(last).append("[H:").append(last.height).append("]");
           System.out.println("The path with the lowest peak is: \n" + sb + "\n");
           return lowestPathList;
       }

   }
} 
